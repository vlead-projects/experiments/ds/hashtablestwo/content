var mkExpParser = function(document) {
  var allObjects = {};
  var expParser = {};

  var getObjId = function(obj) {
    return Object.keys(obj)[0];
  };
  var makeArray = function(htmlCollection) {
    return Array.from(htmlCollection);
  };
  var stripSpans = function(el) {
    try {
      var spanEls = Array.from(el.getElementsByTagName('span'));
      spanEls.forEach(function(item) {
        item.remove();
      });
    } catch(err) {
      console.log("span did not exist, error is " + err);
    }
  };
  var getHeader = function(htag, el) {
    console.log("htag = " + htag);
    try {
      var hEl = Array.from(el.getElementsByTagName(htag))[0];
      stripSpans(hEl);
      return hEl.textContent;
    } catch(err) {
      console.log("No header " + htag + " tag exists, error is " + err);
      return "";
    }
  };
  var getContentText = function(className, el) {
    try {
      var matchedEl = Array.from(el.getElementsByClassName(className))[0];
      return Array.from(matchedEl.getElementsByTagName('p'))[0].textContent;
    } catch(err) {
      console.log("text content is not retrieved, error is " + err);
      return "";
    }
  };
    var createPropertyObj = function (className, el) {
      try {
        var propsObj = {};
        var matchedEl = Array.from(el.getElementsByClassName(className))[0];
        var propDrawer = Array.from(matchedEl.getElementsByClassName('example'))[0];
        var propDrawerText = propDrawer.textContent;
        propDrawer.remove();
        var contents = propDrawerText.split("\n");
        contents.forEach(function (prop) {
          var kv;
          if(prop.split(':')[0] == "TIME-SLICE"){
            var time_slice = "{";
            kv = prop.split("{");
            time_slice += kv[1];
            kv[0] = kv[0].split(":")[0];
            propsObj[kv[0]] = time_slice;
          }
          else{
            kv = prop.split(':');
  
          if (kv.length >= 2){
            var url= "";
            if(kv[0] == "URL"){
              for(i=1;i<kv.length;i++){
                url += kv[i];
                if(i==1){
                  url+=":";
                }
              }
              propsObj[kv[0]] = url;         
            }
            if(kv[0] == "TIME-SLICE"){
  
            }
            else if(kv.length == 2)
              propsObj[kv[0]] = kv[1].trim();
          }
        }
        });
        return propsObj;
      } catch (err) {
        console.log("no properties exist, error is " + err);
        return {};
      }
    };
  
  var getInnerDiv = function(className, el) {
    var matchedEl = Array.from(el.getElementsByClassName(className))[0];
    if (matchedEl == undefined) {
      return "";
    } else {
      return matchedEl.innerHTML;
    }
  };
  var createArtifact = function(artifactDom) {
    try {
      var propertyObj = createPropertyObj('outline-text-4', artifactDom);
      var id = propertyObj["CUSTOM_ID"];
      var artifactObj = {};
      artifactObj['id'] = id;
      artifactObj['header'] = getHeader('h4', artifactDom);
      artifactObj['text'] = getContentText('outline-text-4', artifactDom);
      artifactObj['properties'] = propertyObj;
      artifactObj['divHtml'] = getInnerDiv('outline-text-4', artifactDom);
      return artifactObj;
    } catch(err) {
      console.log("artifact is not created, error is " + err);
      return {};
    }
  };
  var createArtifacts = function(tskDom) {
    var artifactObjList = [];
    try {
      var artifactCol = makeArray(tskDom.getElementsByClassName('outline-4'));
      artifactCol.forEach(function(artifactDom) {
        var artifact = createArtifact(artifactDom);
        var id = artifact['id'];
        allObjects[id] = artifact;
        artifactObjList.push(id);
      });
      return artifactObjList;
    } catch(err) {
      console.log("Error creating artifact list, error is " + err);
      return [];
    }
  };
  var createTask = function(tskDom) {
    try {
      var propertyObj = createPropertyObj('outline-text-3', tskDom);
      var id = propertyObj["CUSTOM_ID"];
      var tskObj = {};
      tskObj['id'] = id;
      tskObj['header'] = getHeader('h3', tskDom);
      tskObj['text'] = getContentText('outline-text-3', tskDom);
      tskObj['properties'] = propertyObj;
      tskObj['artifacts'] = createArtifacts(tskDom);
      return tskObj;
    } catch(err) {
      console.log("Task is not created, error is " + err);
      return {};
    }
  };  
  var createTasks = function(luDom) {
    try {
      var tskObjList = [];
      var tskCol = makeArray(luDom.getElementsByClassName('outline-3'));
      tskCol.forEach(function(tskDom) {
        var tsk = createTask(tskDom);
        var id = tsk['id'];
        allObjects[id] = tsk;
        tskObjList.push(id);
      });
      return tskObjList;
    } catch(err) {
      console.log("Task list is not created, error is " + err);
      return [];
    }
  };
  var createLu = function(luDom) {
    try {
        var propertyObj = createPropertyObj('outline-text-2', luDom);
        var luObj = {};
        if(propertyObj['SCAT']=="expId" || propertyObj['SCAT']=="docType"){
          luObj['id'] = propertyObj['SCAT'];
          luObj['header'] = getHeader('h2', luDom);
          luObj['text'] = propertyObj['CUSTOM_ID'];
          luObj['properties'] = propertyObj;
        }
        else{
          var id = propertyObj["CUSTOM_ID"];
          
          luObj['id'] = id;
          luObj['header'] = getHeader('h2', luDom);
          luObj['text'] = getContentText('outline-text-2', luDom);
          luObj['properties'] = propertyObj;
          luObj['tasks'] = createTasks(luDom);
        }
        return luObj;
      } catch(err) {
        console.log("LU is not created, error is " + err);
        return {};
      }
  };
  var createLUs = function() {
    try {
      var luCol = Array.from(document.getElementsByClassName('outline-2'));
      luCol.forEach(function(luDom) {
        var lu = createLu(luDom);
        var id = lu['id'];
        allObjects[id] = lu;
      });
      return allObjects;
    } catch(err) {
      console.log("LU list is not created, error is " + err);
      return {};
    }
  };

  expParser.createLUs = createLUs;
  return expParser;
};
