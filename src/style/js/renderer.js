var createImgSrc = function(url) {
  var imgTag = createHtmlElement("img");
  imgTag.setAttribute('src', url);

  return imgTag;
};

var createVideoSrc = function(id, startTime, endTime) {
  var url = 'https://www.youtube.com/embed/' + id;
  var ifram = createHtmlElement("iframe");
  ifram.setAttribute("src", url);
  ifram.setAttribute("allowfullscreen", '');
  ifram.style.width = "640px";
  ifram.style.height = "480px";
  return ifram;
}
var createNewIframe = function(url ,id, width, height) {
  var newIframe = document.createElement("iframe");
  newIframe.setAttribute("width", width);
  newIframe.setAttribute("height", height);
  newIframe.setAttribute("frameBorder", "0");
  newIframe.id = id;
  newIframe.src = url;
  return newIframe;
};
var getID = function(url) {
  var id = url.split("&")[0];
  id = id.split("/");
  id = id.pop().split("=");
  return id.pop();
};
var createScriptTag = function(url){
  var script = document.createElement('script');
  script.type = "text/javascript";  
  script.src = ARTEFACT_URL + url;
  return script;
};

var createCssTag = function(url){
  var cssTag = document.createElement('link');
  cssTag.rel = "stylesheet";
  cssTag.type = "text/css";
  cssTag.href = ARTEFACT_URL + url; 
  return cssTag;
};

var createInteractiveJS = function(div, realization){
  var fun = String(realization.creator + "()" + "." + realization.api);
  var tdiv = document.createElement("div");
  tdiv.id = "showjs";
  div.appendChild(tdiv);
  eval(fun)(tdiv);
};
