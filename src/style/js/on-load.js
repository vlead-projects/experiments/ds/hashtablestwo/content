$(document).ready(function() {
    var parser = mkExpParser(document);
    var lus = parser.createLUs();
    var exp_id = lus['expId'].text;
    var doc_type = lus['docType'].text;
    var experimentLUs = $.map(lus, function(value, index){
        return [value];
    });
  
    var settings = {
      'dataType': "json",
      "async": false,
      "url": API_URL + exp_id,
      "method": "GET"
    }
    var catalog;
  	$.ajax(settings).done(function(data){
  		catalog=data;
  	});

    var realization_catalog = JSON.parse(catalog.catalogs[exp_id + "-" + doc_type].realization_catalog);
  
    console.log(experimentLUs);
    console.log(catalog.catalogs);
    console.log(exp_id);
    console.log(realization_catalog);
    
    linker(experimentLUs, catalog.catalogs, exp_id, realization_catalog);

});
