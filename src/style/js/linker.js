var linker = function(experimentLUs, catalog, exp_id, realization_catalog){
  var getObjId = function(obj) {
    return Object.keys(obj)[0];
  };
  var createHtmlElementWithClass = function(el, cls) {
    var elem = document.createElement(el);
    elem.classList.add(cls);
    return elem;
  };
  var createHref = function(id, text) {
    var aTag = createHtmlElement('a');
    aTag.href = '#' + id;
    aTag.innerText = text;
    return aTag;
  };
  var createTasksUl = function(luId, tasks) {
  
    var ul = createHtmlElementWithClass('ul', 'toc-tsk-ul');
  
    if(tasks != undefined){
      tasks.forEach(function(el) {
        experimentLUs.forEach(function(tsk, i) {
          if(tsk['id'] == el){
            var tskLi = createHtmlElementWithClass('li', 'toc-tsk-li');
            
            tskLi.appendChild(createHref(tsk['id'], tsk['header']));
            ul.appendChild(tskLi);
            if(tsk['artifacts'] != undefined){
              var artiLi = createArtifactsUl(tsk['id'], tsk['artifacts']);
              ul.appendChild(artiLi);
            }
          }
        });
      });
      return ul;
    }
  };
  var createArtifactsUl = function(luId, artifacts) {
    
    var ul = createHtmlElementWithClass('ul','toc-arti-ul');
    artifacts.forEach(function(el) {
      experimentLUs.forEach(function(arti) {
        if(arti['id'] == el) {
          var artiLi = createHtmlElementWithClass('li','toc-arti-li');
          
          artiLi.appendChild(createHref(arti['id'], arti['header']));
          ul.appendChild(artiLi);
        }
      });
    });
  
    return ul;
  }
  
  var createLusUl = function() {
    var ul = createHtmlElementWithClass('ul', 'toc-lu-ul');
  
    experimentLUs.forEach(function(el) {
      var id = getObjId(el);
  
      
      if(el['properties']['SCAT'] == "lu" || el['properties']['SCAT'] == "pmbl" || el['properties']['SCAT'] == "module" || el['properties']['SCAT'] == "title") {
        var luLi = createHtmlElementWithClass('li', 'toc-lu-li');  
        luLi.appendChild(createHref(el['id'], el['header']));
        ul.appendChild(luLi);
      
        if(createTasksUl(el['id'], el['tasks']) != undefined) {
  		    luLi.appendChild(createTasksUl(el['id'], el['tasks']));
        }
  	}
      
    });
    return ul;
  };
  var buildToc = function() {
    var toc = createHtmlElementWithClass('div', 'toc');
    var head = createHtmlElementWithClass('h2', 'toc-head');
    head.innerText = 'Table of Contents';
    toc.appendChild(head);
    toc.appendChild(createLusUl());
  
    return toc;
  };
  var createTopDiv  = function() {
    var topDiv = createHtmlElementWithClass('div', 'topDiv');
    topDiv.classList.add('border');
    topDiv.classList.add('row');
    topDiv.classList.add('container-fluid');
    return topDiv;
  };
  var createLeftDiv  = function() {
    var leftDiv = createHtmlElementWithClass('div', 'leftDiv');
    leftDiv.classList.add('border');
    leftDiv.classList.add('col-xs-4');
    leftDiv.classList.add('col-sm-4');
    leftDiv.classList.add('col-md-4');
    leftDiv.classList.add('col-lg-4');
    return leftDiv;
  };
  var createRightDiv  = function() {
    var rightDiv = createHtmlElementWithClass('div', 'rightDiv');
    rightDiv.classList.add('border');
    rightDiv.classList.add('col-xs-8');
    rightDiv.classList.add('col-sm-8');
    rightDiv.classList.add('col-md-8');
    rightDiv.classList.add('col-lg-8');
    return rightDiv;
  };
  var createTwoColumns = function() {
    
    var topDiv = createTopDiv();
    var leftDiv = createLeftDiv();
    var rightDiv = createRightDiv();
  
    topDiv.appendChild(leftDiv);
  
    topDiv.appendChild(rightDiv);
    console.log(topDiv);
    document.body.appendChild(topDiv);
  };
  var attachToc = function() {
    var elem = document.getElementsByClassName('leftDiv')[0];
    elem.appendChild(buildToc());
  };
  var clearCurrentContent = function() {
    if (document.getElementsByClassName('topDiv')[0] != null) {
      var topDiv = document.getElementsByClassName('topDiv')[0];
      topDiv.getElementsByClassName('rightDiv')[0].remove();
    }
  };
  
  var setContentForRight = function(div) {
    var elem = document.getElementsByClassName('rightDiv')[0];
    elem.appendChild(div);
  };
  var showToc = function(hashPath) {
    console.log('showToc');
  };
  var createPrimaryCnt = function(match, header) {
    console.log("Creating Primary Content");
    var div = createHtmlElementWithClass('div', 'cDivCnt');
    var head = createHtmlElementWithClass('h4', 'cDivCntHead');
    head.innerText = header;
    var innerDiv = createHtmlElementWithClass('div', 'cInnerDiv');
  
    if(experimentLUs[match]['divHtml']){
      innerDiv.innerHTML = experimentLUs[match]['divHtml'];
    }
    if(experimentLUs[match]['properties']['SCAT'].toLowerCase() == "reqa"){
  
      if(experimentLUs[match]['properties']['TYPE'].toLowerCase() == "image"){
        let CUSTOM_ID = experimentLUs[match]['properties']['CUSTOM_ID'];
        let realization = realization_catalog[CUSTOM_ID];
        let imgTag = createImgSrc(ARTEFACT_URL + realization['url']);
        innerDiv.appendChild(imgTag);
      }
      if(experimentLUs[match]['properties']['TYPE'].toLowerCase() == "html"){
        let CUSTOM_ID = experimentLUs[match]['properties']['CUSTOM_ID'];
        let realization = realization_catalog[CUSTOM_ID];
        let iframeTag = createNewIframe(ARTEFACT_URL + realization['url'],
                                        CUSTOM_ID, realization['width'],
                                        realization['height']);
        innerDiv.appendChild(iframeTag);
      }
      if(experimentLUs[match]['properties']['TYPE'].toLowerCase() == "interactivejs"){
        let CUSTOM_ID = experimentLUs[match]['properties']['CUSTOM_ID'];
        let realization = realization_catalog[CUSTOM_ID];
        let js_list = realization.url["js"];
        let css_list = realization.url["css"];
  
        for(let i=0; i < js_list.length; i++){
          var scriptTag = createScriptTag(js_list[i]);  
          document.getElementsByTagName("head")[0].appendChild(scriptTag);
        }
  
        for(let i=0; i < css_list.length; i++){
          var cssTag = createCssTag(css_list[i]);  
          document.getElementsByTagName("head")[0].appendChild(cssTag);
        }
  
        $.getScript(ARTEFACT_URL + realization.url["js"][realization.url["js"].length-1], function(){
          createInteractiveJS(innerDiv, realization);
        });
  
      }
    }
  
    if(experimentLUs[match]['properties']['SCAT'] == "imga"){
      var imgTag = createImgSrc(experimentLUs[match]['properties']['URL']);
      innerDiv.appendChild(imgTag);
    }
  
    if(experimentLUs[match]['properties']['SCAT'] == "vida"){
      var url = experimentLUs[match]['properties']['URL'];
      var id = getID(url);
      var startTime=0, endTime=0;
      if(experimentLUs[match]['properties']['TIME_SLICE']){
        startTime = experimentLUs[match]['properties']['TIME_SLICE']['START_TIME'];
        endTime = experimentLUs[match]['properties']['TIME_SLICE']['END_TIME'];
      }
      
      var vidTag = createVideoSrc(id , startTime, endTime);
      innerDiv.appendChild(vidTag);
    }
  
    div.appendChild(head);
    div.appendChild(innerDiv);
    return div;
  };
  var buildContent = function(match) {
    var obj = {};
    obj['primCnt'] = createPrimaryCnt(match, experimentLUs[match]['header']);
  
    return obj;
  };
  var showContent = function(obj) {
    console.log(obj);
    clearCurrentContent();
    var rightDiv = createRightDiv();
    var topDiv = document.getElementsByClassName('topDiv')[0];
    
    topDiv.appendChild(rightDiv);
    console.log(topDiv);
    setContentForRight(obj['primCnt']);
  };
  var changeState = function(hashPath) {
    var match;
    experimentLUs.forEach(function(el, iterator) {
      if(el['id'] == hashPath) {
        match = iterator;
      }
    });
  
    if (match == null) {
      showToc(hashPath);
    } else {
      showContent(buildContent(match));
    }
  };
  function getWindowHashPath() {
    return location.hash.substring(1);
  };
  var setHash = function(self) {
    window.location.hash = self.id;
  };
  var registerOnHashChangeHandler = function() {
    $(window).on("hashchange", function(e) {
      //strip hash out
      var hashPath = getWindowHashPath();
      console.log("new hash: ", hashPath);
      changeState(hashPath);
    });
  };
  function navigate(path) {
    var cu = window.location.href;
    console.log("navigate - path = %s", path);
    console.log("replace - %s", cu.replace(/#(.*)$/, '') + '#' + path);
    var current = window.location.href;
    window.location.href = current.replace(/#(.*)$/, '') + '#' + path;
  };
  var clearBody = function(){
    document.body.innerHTML = '';
  }
  var goAhead = function() {
      clearBody();
      createTwoColumns();
      
      attachToc();
      registerOnHashChangeHandler();
      let currentHash = getWindowHashPath();
      console.log(currentHash);
      if (currentHash === "") {
        currentHash = "myhash";
        console.log("navigating to: #" + currentHash);
        navigate(currentHash);
        $(window).trigger("hashchange");
      }
    };
  goAhead();
};
var createHtmlElement = function(el) {
  var elem = document.createElement(el);
  return elem;
};
